#!/bin/bash
echo "Seting up dotfiles"
echo "Assuming you already installed i3"


echo "Installing dependencies..."

sudo pacman -S zsh git picom dunst rofi alacritty sddm base-devel

echo "Installing yay for AUR dependencies"

git clone https://aur.archlinux.org/yay.git temp/aur/yay
cd temp/aur/yay && makepkg -si
rm -rf temp/aur/yay

echo "Done, installing AUR deps"
yay -S polybar cava spotify polybar-spotiy-module

