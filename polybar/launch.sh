#!/bin/bash

killall -q polybar

while pgrep -u $UUID -x polybar > /dev/null; do sleep 1; done

location ~/.config/polybar/config
polybar example &

echo polybar running
